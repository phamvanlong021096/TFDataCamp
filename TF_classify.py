import os
from skimage import data
from skimage import transform
from skimage.color import rgb2gray
import skimage
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import random

def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory)
                   if os.path.isdir(os.path.join(data_directory, d))]

    images = []
    labels = []
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f)
                      for f in os.listdir(label_directory)
                      if f.endswith(".ppm")]

        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
    return images, labels


ROOT_PATH = 'data'
train_data_directory = os.path.join(ROOT_PATH, "Training")
test_data_directory = os.path.join(ROOT_PATH, "Testing")
images, labels = load_data(train_data_directory)

images_array = np.array(images)
labels_array = np.array(labels)

# Print the `images` dimentions
print(images_array.ndim)

# Print the number of `images`'s elements
print(images_array.size)

# Print the first instance of `images`
images_array[0]

# Print the `labels` dimentions
print(labels_array.ndim)

# Print the number of `label`'s elements
print(labels_array.size)

# Count the number of labels
print(len(set(labels_array)))

# Make a histogram with 62 bins of the `labels` data
# plt.hist(labels, 62)
# plt.show()

# traffic_signs = [300, 2250, 3650, 4000]

# for i in range(len(traffic_signs)):
#     plt.subplot(1, 4, i+1)
#     plt.axis('off')
#     plt.imshow(images[traffic_signs[i]])
#     plt.subplots_adjust(wspace=0.5)
#     plt.show()
#     print("shape: {0}, min: {1}, max: {2}".format(images[traffic_signs[i]].shape,
#                                                   images[traffic_signs[i]].min(),
#                                                   images[traffic_signs[i]].max()))
    
# unique_labels = set(labels)

# plt.figure(figsize=(15, 15))
# i = 1
# for label in unique_labels:
#     image = images[labels.index(label)]
#     plt.subplot(8, 8, i)
#     plt.axis('off')
#     plt.title("Label {0} ({1})".format(label, labels.count(label)))
#     i += 1
#     plt.imshow(image)

# plt.show()

# Rescale the images in the `images` array
images28 = [transform.resize(image, (28, 28)) for image in images]

# Convert `images28` to an array
images28 = np.array(images28)
# Convert `images28` to grayscale
images28 = rgb2gray(images28)

# Initialize placeholders
x = tf.placeholder(dtype=tf.float32, shape=[None, 28, 28])
y = tf.placeholder(dtype=tf.int32, shape=[None])

# Flatten the input data
images_flat = tf.contrib.layers.flatten(x)

# Fully connected layer
layer1 = tf.contrib.layers.fully_connected(images_flat, 128, tf.nn.relu)
logits = tf.contrib.layers.fully_connected(layer1, 62, None)

# Define a loss function
loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits))

# Define an optimizer
train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

# Convert logits to label indexes
correct_pred = tf.arg_max(logits, 1)

# Define an accuracy metric
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# print("images_flat: ", images_flat)
# print("logits: ", logits)
# print("loss: ", loss)
# print("predicted_labels: ", correct_pred)

tf.set_random_seed(1234)

sess = tf.Session()
sess.run(tf.global_variables_initializer())
for i in range(401):
    _, loss_value = sess.run([train_op, loss], feed_dict={x: images28, y: labels})
    if i % 10 == 0:
        print("Loss: ", loss_value)

# Pick 10 random images
# sample_indexes = random.sample(range(len(images28)), 10)
# sample_images = [images28[i] for i in sample_indexes]
# sample_labels = [labels[i] for i in sample_indexes]

# Run the "correct_pred" operation
# predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]

# Print the real and predicted labels
# print(sample_labels)
# print(predicted)

# Display the predictions and the ground truth visually.
# fig = plt.figure(figsize=(10, 10))
# for i in range(len(sample_images)):
#     truth = sample_labels[i]
#     prediction = predicted[i]
#     plt.subplot(5, 2, i+1)
#     plt.axis('off')
#     color = 'green' if truth == prediction else 'red'
#     plt.text(40, 10, "Truth:   {0}\nPrediction: {1}".format(truth, prediction), fontsize=12, color=color)
#     plt.imshow(sample_images[i], cmap="gray")

# plt.show()

# Load the test data
test_images, test_labels = load_data(test_data_directory)

# Transform the images to 28 by 28 pixels
test_images28 = [transform.resize(image, (28, 28)) for image in test_images]

# Convert to grayscale
test_images28 = rgb2gray(np.array(test_images28))

# Run predictions against the full test set
predicted = sess.run([correct_pred], feed_dict={x: test_images28})[0]

# Calculate correct matches
match_count = sum([int(y == y_) for y, y_ in zip(test_labels, predicted)])

# Calculate the accuracy
accuracy_test = match_count / len(test_labels)

# Print the accuracy
print("Accuracy: {:.3f}".format(accuracy_test))

sess.close()
